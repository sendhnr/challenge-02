const prevIcon = '<div class = "prev-slide"><i class = "fa fa-solid fa-chevron-left fa-lg" ></i></div>';
const next = '<div class = "next-slide"><i class = "fa fa-solid fa-chevron-right fa-lg" ></i></div>';

$('.owl-carousel').owlCarousel({
    center: true,
    loop:true,
    margin:10,
    nav:true,
    navText: [
        prevIcon, 
        nextIcon
    ],
    responsive: {
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:2
        }
    }
})